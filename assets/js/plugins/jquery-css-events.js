;(function($) {
"use strict"

var on = $.fn.on
var off = $.fn.off
var one = $.fn.one
var css = $.fn.css

$.fn.on = function() {

	var args = Array.prototype.slice.call(arguments)

	if (args[0] == 'transitionend') {
		args[0] = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
	}

	if (args[0] == 'animationend') {
		args[0] = 'webkitAnimationEnd oanimationend oAnimationEnd msAnimationEnd animationend';
	}

	return on.apply(this, args)
}

$.fn.one = function() {

	var args = Array.prototype.slice.call(arguments)

	if (args[0] == 'transitionend') {
		args[0] = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
	}

	if (args[0] == 'animationend') {
		args[0] = 'webkitAnimationEnd oanimationend oAnimationEnd msAnimationEnd animationend';
	}

	return one.apply(this, args)
}

$.fn.off = function() {

	var args = Array.prototype.slice.call(arguments)

	if (args[0] == 'transitionend') {
		args[0] = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
	}

	if (args[0] == 'animationend') {
		args[0] = 'webkitAnimationEnd oanimationend oAnimationEnd msAnimationEnd animationend';
	}

	return off.apply(this, args)
}

})(jQuery);