(function ($) {

	$.defineControllerMediaQuery('sm', '(min-width: 768px)')
	$.defineControllerMediaQuery('md', '(min-width: 992px)')
	$.defineControllerMediaQuery('lg', '(min-width: 1200px)')

	// iOS orientation
	$(document).ready(function() {
		if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
			var viewportmeta = document.querySelector('meta[name="viewport"]');
			if (viewportmeta) {
				viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
				document.body.addEventListener('gesturestart', function() {
					viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
				}, false);
			}
		}
	});

})(window.jQuery);
