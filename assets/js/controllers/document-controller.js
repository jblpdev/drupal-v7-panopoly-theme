;(function($) {
"use strict"

$.defineController('document-controller', {

	//--------------------------------------------------------------------------
	// Properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Methods
	//--------------------------------------------------------------------------

	/**
	 * Initialize this controller.
	 * @method init
	 * @since 1.0
	 */
	init: function() {

	},

	//--------------------------------------------------------------------------
	// Events
	//--------------------------------------------------------------------------

	/**
	 * Called when the page is fully loaded.
	 * @method onLoad
	 * @since 1.0
	 */
	onLoad: function() {

	},

	/**
	 * Called when all controllers are loaded.
	 * @method onLoad
	 * @since 1.0
	 */
	onReady: function() {

	},

	/**
	 * Called when the window scrolls.
	 * @method onLoad
	 * @since 1.0
	 */
	onScroll: function() {

	}

})

})(jQuery)