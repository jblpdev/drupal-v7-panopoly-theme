<?php

/**
 * Return a single or an array of renderable fields.
 * @function get_field
 * @since 1.0
 */
function get_field($field_name, $field_node = null, $field_type = null, $index = -1) {

	global $base_current_processed_node;

	if ($field_node == null) $field_node = $base_current_processed_node;
	if ($field_type == null) $field_type = 'node';

	if ($field_node == null)
		throw new Exception('get_field: No node given for field ' . $field_name);

	$value = false;

	if ($field = field_info_field($field_name)) {
		$items = get_field_items($field_name, $field_node, $field_type, $field);
		$views = get_field_views($field_name, $field_node, $field_type, $field, $items);
		$value = get_field_value($views, $index, $field);
	}

	return $value;
}

/**
 * Return the render of a specific field.
 * @function get_field_render
 * @since 1.0
 */
function get_field_render($field_name, $field_node = null, $field_type = null, $index = -1) {
	$field = is_array($field_name) ? $field_name : get_field($field_name, $field_node, $field_type, $index);
	return render($field);
}

/**
 * Return the value of a specified field.
 * @function get_value
 * @since 1.0
 */
function get_value($field_name, $field_node = null, $field_type = null, $index = -1) {

	global $base_current_processed_node;

	if ($field_node == null) $field_node = $base_current_processed_node;
	if ($field_type == null) $field_type = 'node';

	if ($field_node == null)
		throw new Exception('get_subfield: No node given for field ' . $field_name);

	$value = false;

	if ($field = field_info_field($field_name)) {
		$items = get_field_items($field_name, $field_node, $field_type, $field);
		$datas = get_field_datas($field_name, $field_node, $field_type, $field, $items);
		$value = get_field_value($datas, $index, $field);
	}

	return $value;
}

/**
 * Indicates whether a value is set for a specified field.
 * @function has_value
 * @since 1.0
 */
function has_value($field_name, $field_node = null, $field_type = null) {

	global $base_current_processed_node;

	if ($field_node == null) $field_node = $base_current_processed_node;
	if ($field_type == null) $field_type = 'node';

	if ($field_node == null)
		throw new Exception('get_subfield: No node given for field ' . $field_name);

	$value = false;

	if ($field = field_info_field($field_name)) {
		$items = get_field_items($field_name, $field_node, $field_type, $field);
		$datas = get_field_datas($field_name, $field_node, $field_type, $field, $items);
		return $field['cardinality'] == -1 || $field['type'] == 'field_collection' ? count($datas) > 0 : isset($datas[0]);
	}

	return $value;
}

/**
 * Return the sub field of a specified field collection item;
 * @function get_subfield
 * @since 1.0
 */
function get_subfield($field_name, &$field_collection_item, $index = -1) {

	if ($field_collection_item == null)
		throw new Exception('get_subfield: No field collection item given for sub field ' . $field_name);

	$value = false;

	if ($field = field_info_field($field_name)) {
		$items = get_field_items($field_name, $field_collection_item, 'field_collection_item', $field);
		$views = get_field_views($field_name, $field_collection_item, 'field_collection_item', $field, $items);
		$value = get_field_value($views, $index, $field);
	}

	return $value;
}

/**
 * Return the sub value of a specified field collection item;
 * @function get_subvalue
 * @since 1.0
 */
function get_subfield_render($field_name, &$field_collection_item) {
	$field = is_array($field_name) ? $field_name : get_subfield($field_name, $field_collection_item);
	return render($field);
}

/**
 * Return the sub value of a specified field collection item;
 * @function get_subvalue
 * @since 1.0
 */
function get_subvalue($field_name, &$field_collection_item, $index = -1) {

	$value = false;

	if ($field = field_info_field($field_name)) {
		$items = get_field_items($field_name, $field_collection_item, 'field_collection_item', $field);
		$datas = get_field_datas($field_name, $field_collection_item, 'field_collection_item', $field, $items);
		$value = get_field_value($datas, $index, $field);
	}

	return $value;
}

/**
 * @function get_field_items()
 * @since 1.0
 */
function get_field_items($field_name, &$field_node, $field_type, &$field) {

	$items = field_get_items($field_type, $field_node, $field_name);

	if ($field['type'] == 'field_collection') {
		$ids = array();
		foreach ($items as $item) $ids[] = $item['value'];
		$items = field_collection_item_load_multiple($ids);
	}

	return $items;
}

/**
 * @function get_field_views()
 * @since 1.0
 */
function get_field_views($field_name, &$field_node, $field_type, &$field, $items) {

	if ($field['type'] == 'field_collection') {
		return $items;
	}

	$values = array();

	if ($items == false) {
		$items = array();
	}

	foreach ($items as $item) $values[] = field_view_value($field_type, $field_node, $field_name, $item);

	return $values;
}

/**
 * @function get_field_datas()
 * @since 1.0
 */
function get_field_datas($field_name, &$field_node, $field_type, &$field, $items) {

	if ($field['type'] == 'field_collection') {
		return $items;
	}

	$values = array();

	if ($items == false) {
		$items = array();
	}

	switch ($field['type']) {

		case 'file':
		case 'image':
			foreach ($items as $item) $values[] = file_create_url($item['uri']);
			break;

		case 'link_field':
			foreach ($items as $item) $values[] = $item['url'];
			break;

		case 'geofield':
			$values = $items;
			break;

		default:
		 	foreach ($items as $item) $values[] = $item['value'];
		 	break;
	}

	return $values;
}

/**
 * @function get_field_value()
 * @since 1.0
 */
function get_field_value($values, $index, &$field) {

	if ($field['type'] == 'field_collection') {
		return $values;
	}

	if ($field['cardinality'] == -1) {

		if ($index > -1) {
			return isset($values[$index]) ? isset($values[$index]) : false;
		}

		return $values;
	}

	return isset($values[0]) ? $values[0] : false;
}
