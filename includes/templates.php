<?php

/**
 * Returns the path of an asset.
 */
function asset($file)
{
	global $base_path;
	return $base_path . drupal_get_path('theme', 'base') . "/assets/$file";
}