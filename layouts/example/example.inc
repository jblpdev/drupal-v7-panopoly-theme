<?php
/**
 * @file
 * Plugin definition.
 */

$plugin = array(
	'title' => t('Example'),
	'icon' => 'example.png',
	'category' => t('Category'),
	'theme' => 'example',
	'css' => 'example.css',
	'regions' => array(
		'zone_1' => t('Zone 1'),
		'zone_2' => t('Zone 2'),
		'zone_3' => t('Zone 3'),
	),
);