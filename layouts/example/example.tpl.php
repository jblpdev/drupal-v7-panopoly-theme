<?php
/**
 * @file
 * Template for JBLP Main.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display panel-layout-example clearfix <?php !empty($class) ? print $class : '' ?>" <?php !empty($css_id) ? print "id=\"$css_id\"" : '' ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-4"><?php print $content['zone_1'] ?></div>
            <div class="col-md-4"><?php print $content['zone_2'] ?></div>
            <div class="col-md-4"><?php print $content['zone_3'] ?></div>
        </div>
    </div>
</div>