Post installation steps
================================================================================

1. Enable base theme
2. Set bartik as admin theme
3. Disable Panopoly demo
4. Check "Use the administration theme when editing or creating content"
