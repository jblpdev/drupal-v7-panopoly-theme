<?php

require_once 'includes/fields.php';
require_once 'includes/templates.php';

/**
 * The current node being processed.
 */
$base_current_processed_node = null;

/**
 * Implimenting hook_process_page()
 */
function base_preprocess_page(&$vars)
{
	global $user;

	// Use node-type and node id based page templates
	if (!empty($vars['node'])) {
		$vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
		$vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->vid;
	}

	if (module_exists('jquery_update') == FALSE) {
		drupal_set_message(t('You must install the <a href="@url">jQuery Update</a> module for base to work properly.', array('@url' => 'https:// drupal.org/project/jquery_update')), 'error');
	}
}

/**
 * Implimenting hook_process_node()
 */
function base_preprocess_node(&$variables, $hook) {
	global $base_current_processed_node;
	$base_current_processed_node = $variables['node'];
}

/**
 * Implimenting hook_html_head_alter()
 */
function base_html_head_alter(&$vars)
{
	// Change the meta content type to HTML5 content type
	$vars['system_meta_content_type']['#attributes'] = array(
		'charset' => 'utf-8'
	);

	// Unsetting the content generator. (Why keep it?)
	unset($vars['system_meta_generator']);

	// Adding the mobile viewport
	$vars['viewport'] = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
			'name' => 'viewport',
			'content' => 'width=device-width, initial-scale=1.0',
		)
	);
}
