<div class="container">

	<?php if (!empty($page['header'])): ?>
		<?php echo render($page['header']) ?>
	<?php endif ?>

	<article class="main-content" role="article">

		<?php echo render($title_prefix) ?>

		<?php if ($title): ?>
			<div class="page-header">
				<h1><?php echo $title ?></h1>
			</div>
		<?php endif ?>

		<?php echo render($title_suffix) ?>

		<?php if ($breadcrumb): ?>
			<?php echo $breadcrumb ?>
		<?php endif ?>

		<?php if ($tabs): ?>
			<?php echo render($tabs) ?>
		<?php endif ?>

		<?php if ($messages): ?>
			<?php echo $messages ?>
		<?php endif ?>

		<?php if ($action_links): ?>
			<ul class="action-links">
				<?php echo render($action_links) ?>
			</ul>
		<?php endif ?>

		<?php echo render($page['content']) ?>

	</article>

	<?php if (!empty($page['footer'])): ?>
		<?php echo render($page['footer']) ?>
	<?php endif ?>

</div>